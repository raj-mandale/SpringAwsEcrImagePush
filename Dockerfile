FROM openjdk:17-alpine
EXPOSE 8080
ADD target/dockerdemo.jar dockerdemo
ENTRYPOINT ["java", "-jar","dockerdemo"]
